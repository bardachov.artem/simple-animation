jQuery(document).ready(function($) {
    // инициализация дропдауна
    $('a.choose-lang-control').click(function(e) {
        e.preventDefault();
        $('.languages-list').stop().slideToggle(400);
    });

    // события изминения числового поля
    $('.control-minus').click(function(event) {
        var number_field = $(this).siblings('input[type="number"]');    //числовое поле в блоке
        var currentQuantity = parseFloat(number_field.val());           //текущее значение поля

        if(currentQuantity == 1) {
            return false
        }
        
        number_field.val(currentQuantity - 1)
    });

    $('.control-plus').click(function(event) {
        var number_field = $(this).siblings('input[type="number"]');

        number_field.val(parseFloat(number_field.val()) + 1)
    });
});