jQuery(document).ready(function($) {
    setTimeout(function(){
        $('#slideFormShowControl').addClass('visible')
    }, 2000);

    $('#slideFormShowControl').click(function(event) {
        $(this).removeClass('visible');
        $('#slideForm').addClass('visible');
    });

    $('#slideFormHideControl').click(function(event) {
        $('#slideForm').removeClass('visible');
        $('#slideFormShowControl').addClass('visible');
    });
});