jQuery(document).ready(function($) {
    setTimeout(function(){
        $('.animation-container').show(0, function() {
            $('#slideForm').addClass('visible')    
        });
    }, 200);

    $('#slideFormHideControl').click(function(event) {
        $('#slideForm').removeClass('visible');
        
        setTimeout(function(){
            $('.animation-container').hide();    
        }, 800);
    });
});