jQuery(document).ready(function($) {
    $('.selectize').selectize({
        create: false,
        createOnBlur: true,
        persist: false,
        dropdownParent: null
    });
});