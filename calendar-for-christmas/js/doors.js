
jQuery(document).ready(function($) {
    stylizeDoors();
    updateDoorsHeight();
    activateDoors(new Date());

    $(window).resize(function(event) {
        updateDoorsHeight ()
    });
    
});

function updateDoorsHeight () {
    var width =  $('article.present').width();
    $('article.present').css('height', width);

    console.log($('.container').height())
    console.log($('.container').closest('iframe').height())
}

function stylizeDoors () {
    var styles = '<style type="text/css">.present_pane{background: linear-gradient(135deg, '+colors.doorColor.light+' 50%, '+colors.doorColor.dark+' 50%)} .present_content{background: '+colors.doorBackground+'} .present_date{background: '+colors.dateBackgorund+'; color: '+colors.dateColor+'} .present_bauble{background: '+colors.baubleBackground+'} .present_bauble::before{border-color: '+colors.baubleBackground+'}</style>';
    
    $('head').append(styles);
}

function addLinks (count) {
    $('ul.doors').find('li').each(function(index, el) {
        if (index == count) {return false}
        
        if(links[index] && images[index]) {
            $(el).find('div.present_bauble').append('<a class="update-modal" target="_blank" href="'+links[index]+'"><img src="'+images[index]+'" ></a>');;
        }
    });
}

function activateDoors (today) {

    var styles = '';
    var year = today.getFullYear();
    var month = today.getMonth()+1;
    var day = today.getDate();

    if(test_mode)
    {
        // test_mode
        if (fake_data_setting.year > date_setting.year) 
        {
            styles = '<style type="text/css">ul.doors > li .present .present_pane {transform: rotateY(-97deg);perspective-origin: 0;transition: all .25s ease-in;}</style>'
            addLinks(23);
        } 
        else if (fake_data_setting.year == date_setting.year) 
        {
            if (fake_data_setting.month > date_setting.month) 
            {
                styles = '<style type="text/css">ul.doors > li .present .present_pane {transform: rotateY(-97deg);perspective-origin: 0;transition: all .25s ease-in;}</style>'
                addLinks(23)
            } 
            else if (fake_data_setting.month == date_setting.month) 
            {
                styles = '<style type="text/css">ul.doors > li:nth-child(-n+'+fake_data_setting.day+') .present:hover .present_pane {cursor:pointer;transform: rotateY(-97deg);perspective-origin: 0;transition: all .25s ease-in;}</style>'
                addLinks(fake_data_setting.day);
                styles += '<style type="text/css">ul.doors > li:nth-child(-n+'+(fake_data_setting.day-1)+') .present .present_pane{display:none}</style>' 
            }
        }
    }
    else 
    {
        if (year > date_setting.year) 
        {
            styles = '<style type="text/css">ul.doors > li .present .present_pane {transform: rotateY(-97deg);perspective-origin: 0;transition: all .25s ease-in;}</style>'
            addLinks(23);
        } 
        else if (year == date_setting.year) 
        {
            if (month > date_setting.month) 
            {
                styles = '<style type="text/css">ul.doors > li .present .present_pane {transform: rotateY(-97deg);perspective-origin: 0;transition: all .25s ease-in;}</style>'
                addLinks(23);
            } 
            else if (month == date_setting.month) 
            {
                styles = '<style type="text/css">ul.doors > li:nth-child(-n+'+day+') .present:hover .present_pane {cursor:pointer;transform: rotateY(-97deg);perspective-origin: 0;transition: all .25s ease-in;}</style>'
                addLinks(day);
                styles += '<style type="text/css">ul.doors > li:nth-child(-n+'+(day-1)+') .present .present_pane{display:none}</style>' 
            }
        }
    }
    
    
    $('head').append(styles);
    $('span.date').html('<b>compareMonth:</b>'+date_setting.month+'. <b>compareYear:</b>'+date_setting.year+' <b>fakeDate:</b>'+fake_data_setting.day+'.'+fake_data_setting.month+'.'+fake_data_setting.year+' <b>realDate:</b>'+day+'.'+month+'.'+year);
}