// you may change all colors here
const colors = {
    doorColor: {
        light: '#009688',
        dark: '#009688'
    },
    doorBackground: '#00E676',
    dateColor: '#fff',
    dateBackgorund: '#00E676',
    baubleBackground: '#00796B'
};

// links setting. Should be 24 strings 
const links = [
    'http://1.com',
    'http://2.com',
    'http://3.com'
];

// links setting. Should be 24 strings 
// You should push here full path to image or you may push url
const images = [
    'images/bauble_1.png',
    'images/bauble_2.png',
    'images/bauble_3.png'
];

//This is date for comparison with real or fake(in test mode) date
const date_setting = {
    year: 2017,
    month: 11
};

//This is fake date for testing
const fake_data_setting = {
    year: 2017,
    month: 11,
    day: 3
};

// mode switch
const test_mode = true; //true/false